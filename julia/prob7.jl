### A Pluto.jl notebook ###
# v0.12.20

using Markdown
using InteractiveUtils

# ╔═╡ c2fa4834-7dd8-11eb-142a-c1ead956587b
N = 10000

# ╔═╡ 94ad26ae-7dd8-11eb-2962-77e3a4723200
primes = let primes = [2], is_prime = false
	for i in range(3, stop=N)
		is_prime = true
		sqrt_i = sqrt(i)
		for p in primes
			if i % p == 0
				is_prime = false
			end
			if p > sqrt_i
				break
			end
		end
		if is_prime
			append!(primes, i)
		end
	end
	primes
end

# ╔═╡ d04653ce-7dd9-11eb-384c-190f8ffecf71
m = 600851475143

# ╔═╡ e01dcc32-7dd9-11eb-0470-03cb9df92017
divisors, dividend = let divisors_m = [], dividend = m
	for p in primes
		while dividend % p == 0
			append!(divisors_m, p)
			dividend = dividend / p
		end
		if p > dividend
			break
		end
	end
	divisors_m, dividend
end

# ╔═╡ edf6ffe4-7dda-11eb-26dc-ffd22ec133bc
@assert dividend == 1.0

# ╔═╡ Cell order:
# ╠═c2fa4834-7dd8-11eb-142a-c1ead956587b
# ╠═94ad26ae-7dd8-11eb-2962-77e3a4723200
# ╠═d04653ce-7dd9-11eb-384c-190f8ffecf71
# ╠═e01dcc32-7dd9-11eb-0470-03cb9df92017
# ╠═edf6ffe4-7dda-11eb-26dc-ffd22ec133bc
