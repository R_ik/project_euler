### A Pluto.jl notebook ###
# v0.12.20

using Markdown
using InteractiveUtils

# ╔═╡ 6b0505a0-8121-11eb-38e9-61df4d3c6ea6
palindromes = let palindromes = []
	for i in range(1, stop=1000)
		for j in range(i, stop=1000)
			prod = string(i*j)
			if prod == reverse(prod)
				append!(palindromes, [prod])
			end
		end
	end
	[parse(Int, x) for x in palindromes]
end

# ╔═╡ 563839f4-8122-11eb-26ee-534099fd2f03
answer = maximum(palindromes)

# ╔═╡ Cell order:
# ╠═6b0505a0-8121-11eb-38e9-61df4d3c6ea6
# ╠═563839f4-8122-11eb-26ee-534099fd2f03
