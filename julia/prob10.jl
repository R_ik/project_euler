### A Pluto.jl notebook ###
# v0.12.20

using Markdown
using InteractiveUtils

# ╔═╡ 17c9f3de-81bf-11eb-27f4-8b6e5f7307f0
N = 2000000

# ╔═╡ 122f8efc-81bf-11eb-1689-e10e144e7758
primes = let primes = [2], is_prime = false
	for i in range(3, stop=N)
		is_prime = true
		sqrt_i = sqrt(i)
		for p in primes
			if i % p == 0
				is_prime = false
			end
			if p > sqrt_i
				break
			end
		end
		if is_prime
			append!(primes, i)
		end
	end
	primes
end

# ╔═╡ 8741151c-81bf-11eb-285f-6793811c505c
answer = sum(primes)

# ╔═╡ Cell order:
# ╠═17c9f3de-81bf-11eb-27f4-8b6e5f7307f0
# ╠═122f8efc-81bf-11eb-1689-e10e144e7758
# ╠═8741151c-81bf-11eb-285f-6793811c505c
