### A Pluto.jl notebook ###
# v0.12.20

using Markdown
using InteractiveUtils

# ╔═╡ 6dda2db8-8127-11eb-22db-e1aaa4096806
N = 13

# ╔═╡ 86a8d502-8126-11eb-22e8-7d121be8d637
raw_data = readlines(open("input.txt"))

# ╔═╡ dc723c08-8126-11eb-0779-7f920d4ff79d
full_number = join(raw_data, "")

# ╔═╡ b328a040-8127-11eb-31bb-f5b7bbaf0cf7
to_int_list = int_list -> map(x->parse(Int, x), collect(int_list))

# ╔═╡ 0247ee32-8127-11eb-0eb8-a92c5cf3b088
answer = let largest_prod = 0
	for i = 1:length(full_number)-N-1
		digits = to_int_list(full_number[i:i+N-1])
		if prod(digits) > largest_prod
			largest_prod = prod(digits)
		end
	end
	largest_prod
end

# ╔═╡ Cell order:
# ╠═6dda2db8-8127-11eb-22db-e1aaa4096806
# ╠═86a8d502-8126-11eb-22e8-7d121be8d637
# ╠═dc723c08-8126-11eb-0779-7f920d4ff79d
# ╠═b328a040-8127-11eb-31bb-f5b7bbaf0cf7
# ╠═0247ee32-8127-11eb-0eb8-a92c5cf3b088
