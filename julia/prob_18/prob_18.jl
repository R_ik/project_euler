### A Pluto.jl notebook ###
# v0.12.20

using Markdown
using InteractiveUtils

# ╔═╡ 47e890bc-7688-11eb-107c-d7c6b95c58f3
cd("/Users/rvlist/Documents/misc/project_euler/julia/prob_18")

# ╔═╡ 89e65b7e-768e-11eb-3f40-8d3c910dd087
raw_data = readlines(open("triangle.txt"))

# ╔═╡ 9dff8a36-768e-11eb-3ce4-452e5f49e439
parse_line = (line) -> map((i) -> parse(Int, i), split(line, " "))

# ╔═╡ be83ee8e-768e-11eb-036b-879356de4fd0
data = map(parse_line, raw_data)

# ╔═╡ ce658fa4-768e-11eb-2127-69f93e45c669
answer = let last_row = [], max_sum = []
	for i in range(1, stop=length(data))
		show(i)
		parent_right = [0; last_row]
		parent_left = [last_row; 0]
		parents = [parent_left parent_right]
		best_parents = maximum(parents, dims=2)
		current_best = data[i] .+ best_parents
		last_row = current_best
		append!(max_sum, [current_best])
	end
	maximum(max_sum[end])
end

# ╔═╡ Cell order:
# ╠═47e890bc-7688-11eb-107c-d7c6b95c58f3
# ╠═89e65b7e-768e-11eb-3f40-8d3c910dd087
# ╠═9dff8a36-768e-11eb-3ce4-452e5f49e439
# ╠═be83ee8e-768e-11eb-036b-879356de4fd0
# ╠═ce658fa4-768e-11eb-2127-69f93e45c669
