### A Pluto.jl notebook ###
# v0.12.20

using Markdown
using InteractiveUtils

# ╔═╡ 63a6b282-8175-11eb-0845-ed2a39d2c263
N = 1000

# ╔═╡ d0bd6030-8172-11eb-3e60-b9595c9ca487
b = Array(1:N÷3-1)

# ╔═╡ dbff9076-81bd-11eb-02de-2f6e3d046438
a = (N^2 .- 2*N*b) ./ (2*N .- 2*b)

# ╔═╡ 816ebf70-8178-11eb-17a6-954a63857bb8
valid_ab_pairs = [(round.(Int, a), b) for (b,a) in enumerate(a) if ceil(a) == a]

# ╔═╡ 00a2a74e-8174-11eb-281b-15a939831f62
answers = map(tuple -> let (b,a) = tuple; a*b*(N-a-b); end, valid_ab_pairs)

# ╔═╡ Cell order:
# ╠═63a6b282-8175-11eb-0845-ed2a39d2c263
# ╠═d0bd6030-8172-11eb-3e60-b9595c9ca487
# ╠═dbff9076-81bd-11eb-02de-2f6e3d046438
# ╠═816ebf70-8178-11eb-17a6-954a63857bb8
# ╠═00a2a74e-8174-11eb-281b-15a939831f62
