### A Pluto.jl notebook ###
# v0.12.20

using Markdown
using InteractiveUtils

# ╔═╡ d3f73866-7dd7-11eb-298d-4fe2d8739625
N = 4000000

# ╔═╡ a550d97a-7dd7-11eb-1d03-d956079048a9
answer = let fib = [0,1], total = 0
	while fib[2] < N
		new_value = sum(fib)
		if new_value % 2 == 0 
			total += new_value
		end
		fib = [fib[2] new_value]
	end
	total
end

# ╔═╡ Cell order:
# ╠═d3f73866-7dd7-11eb-298d-4fe2d8739625
# ╠═a550d97a-7dd7-11eb-1d03-d956079048a9
