### A Pluto.jl notebook ###
# v0.12.20

using Markdown
using InteractiveUtils

# ╔═╡ 00ca2dbc-8123-11eb-29d0-8dbcfe1cf625
N = 20

# ╔═╡ e3510fee-8122-11eb-2593-51d0b9e1bd35
primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]

# ╔═╡ 36407478-8124-11eb-0aac-390fb82ddb33
required_prime_factors = let prime_factors = [], x = 0
	for p in primes
		x = p
		while x < N
			append!(prime_factors, p)
			x = x*p
		end
	end
	prime_factors
end

# ╔═╡ bd51a450-8124-11eb-2c72-a94e1dbd8ba5
answer = prod(required_prime_factors)

# ╔═╡ Cell order:
# ╠═00ca2dbc-8123-11eb-29d0-8dbcfe1cf625
# ╠═e3510fee-8122-11eb-2593-51d0b9e1bd35
# ╠═36407478-8124-11eb-0aac-390fb82ddb33
# ╠═bd51a450-8124-11eb-2c72-a94e1dbd8ba5
