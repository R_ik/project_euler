### A Pluto.jl notebook ###
# v0.12.20

using Markdown
using InteractiveUtils

# ╔═╡ 27fb2b6e-8125-11eb-3183-bb94a1cd591b
N = 100

# ╔═╡ 32cb0494-8125-11eb-111e-ad39f713bf87
sum_sq = sum(map(x -> x*x, 1:N))

# ╔═╡ 51e21994-8125-11eb-13d3-a90fb9b2491d
sq_sum = sum(1:N)^2

# ╔═╡ 601db598-8125-11eb-3da4-b733956969f1
answer = sq_sum - sum_sq

# ╔═╡ Cell order:
# ╠═27fb2b6e-8125-11eb-3183-bb94a1cd591b
# ╠═32cb0494-8125-11eb-111e-ad39f713bf87
# ╠═51e21994-8125-11eb-13d3-a90fb9b2491d
# ╠═601db598-8125-11eb-3da4-b733956969f1
