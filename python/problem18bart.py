"""
Solution to problem 18
"""

f = open("problem18.txt", "r")
nums = []
for line in f.readlines():
    numline = []
    while line != "":
        numline.append(int(line[0:2]))
        line = line[3:]
    nums.append(numline)
n = len(nums)

maxold = nums[n-1]
for i in range(1,n):
    row = n-1-i
    maxnew = []
    for j in range(row+1):
        m = nums[row][j]+max(maxold[j],maxold[j+1])
        maxnew.append(m)
    maxold=maxnew
print(maxnew[0])